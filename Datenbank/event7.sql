SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `events` DEFAULT CHARACTER SET utf8 ;
USE `events` ;

-- -----------------------------------------------------
-- Table `events`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `events`.`location` (
  `idLocation` INT(11) NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NOT NULL,
  `zipcode` INT(11) NOT NULL,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `number` INT NOT NULL,
  PRIMARY KEY (`idLocation`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `events`.`event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `events`.`event` (
  `idEvent` INT(11) NOT NULL AUTO_INCREMENT,
  `location_idLocation` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `duration` INT(11) NOT NULL,
  `deleted` INT(11) NULL,
  `maxAp` INT(11) NOT NULL,
  `begin` TIME NOT NULL,
  `end` TIME NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `day` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEvent`),
  INDEX `fk_event_location_idx` (`location_idLocation` ASC),
  CONSTRAINT `fk_event_location`
    FOREIGN KEY (`location_idLocation`)
    REFERENCES `events`.`location` (`idLocation`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `events`.`connect`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `events`.`connect` (
  `leader` TINYINT(1) NOT NULL,
  `event_idEvent` INT(11) NOT NULL,
  INDEX `fk_connect_event1_idx` (`event_idEvent` ASC),
  CONSTRAINT `fk_connect_event1`
    FOREIGN KEY (`event_idEvent`)
    REFERENCES `events`.`event` (`idEvent`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
