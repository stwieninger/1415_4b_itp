SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `projectmarketing` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `projectmarketing` ;

-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_projecttype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_projecttype` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `Name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_subject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `short` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_department` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `short` VARCHAR(10) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_project` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `class` VARCHAR(20) NOT NULL,
  `year` VARCHAR(20) NOT NULL,
  `student_in_project_ID` INT NOT NULL,
  `teacher_ID` INT NOT NULL,
  `subject_ID` INT NOT NULL,
  `marketingtext` LONGTEXT NULL,
  `text` LONGTEXT NULL,
  `released` TINYINT NULL DEFAULT 0,
  `department_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_project_projecttype_idx` (`student_in_project_ID` ASC),
  INDEX `fk_project_subject1_idx` (`subject_ID` ASC),
  INDEX `fk_project_department1_idx` (`department_id` ASC),
  CONSTRAINT `fk_project_projecttype`
    FOREIGN KEY (`student_in_project_ID`)
    REFERENCES `projectmarketing`.`pm_projecttype` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_subject1`
    FOREIGN KEY (`subject_ID`)
    REFERENCES `projectmarketing`.`pm_subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `projectmarketing`.`pm_department` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_company` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `imagepath` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_partner`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_partner` (
  `project_id` INT NOT NULL,
  `company_id` INT NOT NULL,
  PRIMARY KEY (`project_id`, `company_id`),
  INDEX `fk_project_has_Firmen_Firmen1_idx` (`company_id` ASC),
  INDEX `fk_project_has_Firmen_project1_idx` (`project_id` ASC),
  CONSTRAINT `fk_project_has_Firmen_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `projectmarketing`.`pm_project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_Firmen_Firmen1`
    FOREIGN KEY (`company_id`)
    REFERENCES `projectmarketing`.`pm_company` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `rights` ENUM('student', 'teacher', 'admin') NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `title` VARCHAR(45) NULL,
  `changepwd` TINYINT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(45) NOT NULL,
  `project_ID` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_image_project1_idx` (`project_ID` ASC),
  CONSTRAINT `fk_image_project1`
    FOREIGN KEY (`project_ID`)
    REFERENCES `projectmarketing`.`pm_project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`pm_project_has_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`pm_project_has_user` (
  `project_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`project_id`, `user_id`),
  INDEX `fk_project_has_user_user1_idx` (`user_id` ASC),
  INDEX `fk_project_has_user_project1_idx` (`project_id` ASC),
  CONSTRAINT `fk_project_has_user_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `projectmarketing`.`pm_project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_user_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `projectmarketing`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`class`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`class` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`user_has_class`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`user_has_class` (
  `user_id` INT NOT NULL,
  `class_id` INT NOT NULL,
  PRIMARY KEY (`user_id`, `class_id`),
  INDEX `fk_user_has_class_class1_idx` (`class_id` ASC),
  INDEX `fk_user_has_class_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_has_class_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `projectmarketing`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_class_class1`
    FOREIGN KEY (`class_id`)
    REFERENCES `projectmarketing`.`class` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`em_location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`em_location` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NULL,
  `zip` VARCHAR(10) NULL,
  `city` VARCHAR(45) NULL,
  `number` VARCHAR(10) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`em_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`em_event` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `duration` TINYINT NULL,
  `deleted` TINYINT NULL,
  `maxusers` INT NULL,
  `beginning` TIME NULL,
  `ending` TIME NULL,
  `description` VARCHAR(45) NULL,
  `location_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_location1_idx` (`location_id` ASC),
  CONSTRAINT `fk_event_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `projectmarketing`.`em_location` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `projectmarketing`.`em_event_has_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projectmarketing`.`em_event_has_user` (
  `event_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`event_id`, `user_id`),
  INDEX `fk_event_has_user_user1_idx` (`user_id` ASC),
  INDEX `fk_event_has_user_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_has_user_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `projectmarketing`.`em_event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_user_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `projectmarketing`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
