SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Person`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Person` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Vorname` VARCHAR(45) NULL ,
  `Nachname` VARCHAR(45) NULL ,
  `Passwort` VARCHAR(45) NULL ,
  `Email` VARCHAR(45) NULL ,
  `Telefonnummer` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Klasse`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Klasse` (
  `idKlasse` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(45) NULL ,
  PRIMARY KEY (`idKlasse`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Person`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Person` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Vorname` VARCHAR(45) NULL ,
  `Nachname` VARCHAR(45) NULL ,
  `Passwort` VARCHAR(45) NULL ,
  `Email` VARCHAR(45) NULL ,
  `Telefonnummer` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Schueler`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Schueler` (
  `idSchueler` INT NOT NULL ,
  `Matrikelnummer` VARCHAR(45) NULL ,
  `Klasse_idKlasse` INT NOT NULL ,
  `Person_id` INT NOT NULL ,
  PRIMARY KEY (`idSchueler`, `Person_id`) ,
  INDEX `fk_Schueler_Klasse_idx` (`Klasse_idKlasse` ASC) ,
  INDEX `fk_Schueler_Person1_idx` (`Person_id` ASC) ,
  CONSTRAINT `fk_Schueler_Klasse`
    FOREIGN KEY (`Klasse_idKlasse` )
    REFERENCES `mydb`.`Klasse` (`idKlasse` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Schueler_Person1`
    FOREIGN KEY (`Person_id` )
    REFERENCES `mydb`.`Person` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Lehrer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Lehrer` (
  `idLehrer` INT NOT NULL AUTO_INCREMENT ,
  `LehrerLog` VARCHAR(45) NULL ,
  `Person_id` INT NOT NULL ,
  PRIMARY KEY (`idLehrer`, `Person_id`) ,
  INDEX `fk_Lehrer_Person1_idx` (`Person_id` ASC) ,
  CONSTRAINT `fk_Lehrer_Person1`
    FOREIGN KEY (`Person_id` )
    REFERENCES `mydb`.`Person` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Ort`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Ort` (
  `idOrt` INT NOT NULL AUTO_INCREMENT ,
  `Land` VARCHAR(45) NULL ,
  `Ortsname` VARCHAR(45) NULL ,
  PRIMARY KEY (`idOrt`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Event` (
  `idEvent` INT NOT NULL ,
  `Name` VARCHAR(45) NULL ,
  `Dauer` INT NULL ,
  `Ort_idOrt` INT NOT NULL ,
  `Deleted` INT NULL ,
  PRIMARY KEY (`idEvent`) ,
  INDEX `fk_Event_Ort1_idx` (`Ort_idOrt` ASC) ,
  CONSTRAINT `fk_Event_Ort1`
    FOREIGN KEY (`Ort_idOrt` )
    REFERENCES `mydb`.`Ort` (`idOrt` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Event_has_Lehrer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Event_has_Lehrer` (
  `Event_idEvent` INT NOT NULL ,
  `Lehrer_idLehrer` INT NOT NULL ,
  PRIMARY KEY (`Event_idEvent`, `Lehrer_idLehrer`) ,
  INDEX `fk_Event_has_Lehrer_Lehrer1_idx` (`Lehrer_idLehrer` ASC) ,
  INDEX `fk_Event_has_Lehrer_Event1_idx` (`Event_idEvent` ASC) ,
  CONSTRAINT `fk_Event_has_Lehrer_Event1`
    FOREIGN KEY (`Event_idEvent` )
    REFERENCES `mydb`.`Event` (`idEvent` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Event_has_Lehrer_Lehrer1`
    FOREIGN KEY (`Lehrer_idLehrer` )
    REFERENCES `mydb`.`Lehrer` (`idLehrer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Schueler_has_Lehrer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `event`.`Schueler_has_Lehrer` (
  `Schueler_idSchueler` INT NOT NULL ,
  `Lehrer_idLehrer` INT NOT NULL ,
  PRIMARY KEY (`Schueler_idSchueler`, `Lehrer_idLehrer`) ,
  INDEX `fk_Schueler_has_Lehrer_Lehrer1_idx` (`Lehrer_idLehrer` ASC) ,
  INDEX `fk_Schueler_has_Lehrer_Schueler1_idx` (`Schueler_idSchueler` ASC) ,
  CONSTRAINT `fk_Schueler_has_Lehrer_Schueler1`
    FOREIGN KEY (`Schueler_idSchueler` )
    REFERENCES `mydb`.`Schueler` (`idSchueler` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Schueler_has_Lehrer_Lehrer1`
    FOREIGN KEY (`Lehrer_idLehrer` )
    REFERENCES `mydb`.`Lehrer` (`idLehrer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `mydb` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
