<DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Detailansicht</title>
    
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/jumbotron-narrow.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
      <div class="header">
        
        <h3 class="text-muted">Event-Detailansicht</h3>
      </div>

      <div class="jumbotron">
        <h1>Eventname</h1>
        <p class="lead">It works.</p>
        
      </div>

      <div class="row marketing">
        <div class="col-lg-6">
          <h4>Subheading</h4>
          <p></p>
          <br></br>
          <br></br>

          <h4>Subheading</h4>
          <p></p>
          <br></br>
          <br></br>
        </div>

        <div class="col-lg-6">
          <h4>Subheading</h4>
          <p></p>
          <br></br>
          <br></br>

          <h4>Subheading</h4>
          <p></p>

          <br></br>
          <h4>   </h4>
          <h5>   </h5>

          <br></br>

          <ul class="nav nav-pills pull-right">

        <br></br>
        
        </nav> </p>
        <button class="btn btn-success">Eintragen</button>
        <button class="btn btn-danger">Austragen</button>
        </div>
        
      </div>

      <footer class="footer">
        
        <p>&copy; HTL Krems</p>
      </footer>

    </div>

  </body>
</html>