<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>CapitalCity Template</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>

<div class="navbar navbar-fixed-top alt" data-spy="affix" data-offset-top="1000">
  <div class="container">
    <div class="navbar-header">
      <a href="#" class="navbar-brand">Home</a>
      <a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
      <ul class="nav navbar-nav">
        <li><a href="#sec1">Section 1</a></li>
        <li><a href="#sec2">Section 2</a></li>
        <li><a href="#sec3">Section 3</a></li>
        <li><a href="#sec4">Section 4</a></li>
        <li><div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
    Dropdown
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
  </ul>
</div></li>

      </ul>
    </div>

   </div>
</div>

<div class="header alt vert">
  <div class="container">
    
    <h1>CapitalCity</h1>
      <p class="lead">Starter Template for Bootstrap 3</p>
      <div>&nbsp;</div>
  </div>
</div>

<div id="sec1" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Section1</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div id="sec2" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Section2</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div id="sec3" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Section3</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        <p class="lead">The Most Popular Responsive Framework</p>
        
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div id="sec4" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Section4</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div id="sec5" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Section5</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h1><i class="icon-plane icon-3x"></i></h1>
      </div>
      <div class="col-md-7">
        <h1 class="pull-right">Launch Your Web Design</h1>
        <p class="lead pull-right">With this Responsive Bootstrap Template</p>
      </div>
    </div>
  </div>
</div>

<hr>

<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <ul class="list-inline">
          <li><i class="icon-facebook icon-2x"></i></li>
          <li><i class="icon-twitter icon-2x"></i></li>
          <li><i class="icon-google-plus icon-2x"></i></li>
          <li><i class="icon-pinterest icon-2x"></i></li>
        </ul>
        <hr>
        <p>Built with <i class="icon-heart-empty"></i> at <a href="http://www.bootply.com">Bootply</a>.<br>Company ©2014</p>
      </div>
    </div>
  </div>
</footer>

<ul class="nav pull-right scroll-down">
  <li><a href="#" title="Scroll down"><i class="icon-chevron-down icon-3x"></i></a></li>
</ul>
<ul class="nav pull-right scroll-top">
  <li><a href="#" title="Scroll to top"><i class="icon-chevron-up icon-3x"></i></a></li>
</ul>

	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>