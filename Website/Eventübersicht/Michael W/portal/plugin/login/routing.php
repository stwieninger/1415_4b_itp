<?php

switch (Portal::getParameterId()) {
    default:
        Portal::setPagetitle('Login');

        if (Portal::getRequestMethod() == 'POST')
            Portal::setPage('login-handler.php');
        else
            Portal::setPage('login.php');
        break;
}