<div class="row">
    <div class="col-sm-offset-3 col-sm-6 col-lg-offset-4 col-lg-4 col-xs-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Schulportal - HTL Krems</h3>
            </div>
            <form class="form-horizontal" method="post" action="">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="demo-hor-inputemail" class="col-sm-3 control-label">Benutzername</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="demo-hor-inputemail" placeholder="Benutzername">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="demo-hor-inputpass" class="col-sm-3 control-label">Passwort</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" id="demo-hor-inputpass" placeholder="Passwort">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <label class="form-checkbox form-normal form-primary form-text">
                                <input type="checkbox"> Eingeloggt bleiben
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-info">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
