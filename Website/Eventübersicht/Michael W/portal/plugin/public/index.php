<?php

/*
 * Name: Public
 * Description: Main plugin that will be displayed when the user access the portal
 * Author: Mathias Schuepany
 * Date: 14.03.2015
 */

Portal::setPluginDisplayName('Public'); //name of the plugin - this name will be displayed in the menu as root node name