<?php

//MENÜAUFBAU -> key: name, der im menü angezeigt wird;
//              value: id auf die geroutet wird
$MENU = array("Menuelement" => "start");

/*
 * Portal::displayTemplate(true); // template anzeigen oder nicht (wenn das Template 'false' ist, dann wird nur der Inhalt des Plugins angezeigt)
 * Portal::addJs(new Js('hallo.js')); // Einbinden von eigenem Javascript - Die JS Datei muss in das Verzeichnis 'static/js' kopiert werden
 * Portal::addCss(new Css('hallo.css')); // Einbinden von eigenem CSS - Die CSS Datei muss in das Verzeichnis 'static/css' kopiert werden
 * Portal::setPage('start.php'); // definiert die Datei, die vom Portal eingebunden werden soll (die Dateien liegen im Verzeichnis 'pages' im plugin verzeichnis
 * Portal::setPagetitle('Test'); // Seitentitel
 */

switch (Portal::getParameterId()) {
        case 'add':
            Portal::setPage('add.php');
            break;
        default:
            Portal::displayTemplate(true);
            Portal::addJs(new Js('hallo.js'));
            Portal::addCss(new Css('hallo.css'));
            Portal::setPage('start.php');
            Portal::setPagetitle('Test');
            break;
}