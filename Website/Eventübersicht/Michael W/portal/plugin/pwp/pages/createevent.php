<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CreateEvents</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">


<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<body>

    <div class="container">
      <div class="header">
        
        <h3 class="text-muted">Event-Detailansicht</h3>
      </div>
      <hr>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Event Name</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="name" class="form-control input-md">
  <span class="help-block"></span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Beschreibung</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="description" class="form-control input-md">
  <span class="help-block"></span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Ort</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="destination" class="form-control input-md">
  <span class="help-block"></span>  
  </div>
</div>



<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Wochentag</label>
  <div class="col-md-4">
    <select id="selectbasic" name="selectbasic" class="form-control">
      <option value="1">Montag</option>
      <option value="2">Dienstag</option>
      <option value="2">Mittwoch</option>
      <option value="2">Donnerstag</option>
      <option value="2">Freitag</option>
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
    <button id="button1id" name="button1id" class="btn btn-success">Event erstellen</button>
    
  </div>
</div>

</fieldset>
</form>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!--<script src="./assets/js/docs.min.js"></script>-->
    <script src="./jquery.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
   <!-- <script src="./assets/js/ie10-viewport-bug-workaround.js"></script>-->
  </body>
</html>
