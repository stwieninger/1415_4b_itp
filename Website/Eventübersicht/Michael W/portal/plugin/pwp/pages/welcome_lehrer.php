<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    require 'dbconnect.php';
    $res = $conn->query ("SELECT * FROM event");
    $tmp = $res->fetchAll();
    ?>
    <meta charset="utf-8">
    <title>Events</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="js/less-1.3.3.min.js"></script-->
    <!--append ‘#!watch’ to the browser URL, then refresh the page. -->

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/favicon.png">

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="functions.js"></script>
</head>

<body>
<br>


<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 center-block">
            <div class="jumbotron well">
                <center><h1>
                        Hallo, KV!
                    </h1>
                    <p>
                        Hier sind deine Events!
                    </p>
                    <?php
                    foreach($tmp as $row)
                    {?>
                        <ul class="nav nav-pills">
                            <li><?php echo'<a href="?modul=pwp&id=detailedview&idE='.$row['idEvent'].'"> '.$row['name'];?></a></li>
                        </ul>
                    <?php }
                    ?>
                    </p>
                </center>
                <div class="btn-group">
                    <a class="btn btn-default" href="?modul=pwp&id=createevent">Event Erstellen</a>
                </div>
                <div class="btn-group">
                    <a class="btn btn-default" href="?modul=pwp&id=classoverview">Meine Klasse</a>
                </div>
            </div>
        </div>
    </div>
</div>
<center>
    <form class="navbar-form navbar-fixed-bottom">
        <div class="btn-group btn-group-lg">
            <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=montag">MO</a>
            <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=dienstag">DI</a>
            <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=mittwoch">MI</a>
            <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=donnerstag">DO</a>
        </div>
    </form>
</center>
</body>
</html>
