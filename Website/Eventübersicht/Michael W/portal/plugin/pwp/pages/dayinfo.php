<!DOCTYPE html>
<html lang="en">
<head>
<?php 
	require "head.php";
	require "dbconnect.php"; 
	$dID = $_GET['dID'];

	$res = $conn->query ("SELECT * FROM event WHERE day = '$dID'");
    $tmp = $res->fetchAll(PDO::FETCH_ASSOC);
?>
</head>
<body>

<?php foreach($tmp as $row){ ?>
    <body><h1 class="ueberschrift"><?php echo $row['day']; ?></h1>
<?php break;} ?>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row">
                <?php foreach($tmp as $row)
                { ?>
				<div class="col-md-3">
					<div class="thumbnail">
						<div class="caption">
							<h3><?php echo $row['name']; ?></h3>
							<p><?php echo $row['description']; ?></p>
							<p align="right"><a class="btn btn-default" href="?modul=pwp&id=detailedview&idE=<?php echo $row['idEvent']; ?>">Details</a> <a class="btn btn-success" type="sumbit"href="#">Einschreiben</a></p>
						</div>
					</div>
				</div>
                <?php } ?>
			</div>
		</div>
	</div>
    <a class="btn btn-info" href="?modul=pwp&id=welcome_lehrer">zurück zu meinen Events</a>
</div>
    <center>
        <form class="navbar-form navbar-fixed-bottom">
            <div class="btn-group btn-group-lg">
                <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=montag">MO</a>
                <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=dienstag">DI</a>
                <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=mittwoch">MI</a>
                <a class="btn btn-default" href="?modul=pwp&id=dayinfo&dID=donnerstag">DO</a>
            </div>
        </form>
    </center>
</body>
</html>
