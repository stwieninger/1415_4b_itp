
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/dashboard.css" rel="stylesheet">
        
        <link rel="shortcut icon" href="./img/favicon.ico">


        <link rel="stylesheet" type="text/css" media="screen" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
        
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

	<script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
	<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/src/js/bootstrap-datetimepicker.js"></script>
    </head>

<form class="form-horizontal">
<fieldset>
<body>
<div class="container">
      <div class="header">
        
        <h3 class="text-muted">Event-Detailansicht</h3>
      </div>
      <hr>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Event Name</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="name" class="form-control input-md">
  <span class="help-block"></span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Beschreibung</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="Beschreibung" class="form-control input-md">
  <span class="help-block"></span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Ort</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="Ort" class="form-control input-md">
  <span class="help-block"></span>  
  </div>
</div>
<div class="form-group">
<label class="col-md-4 control-label" for="textinput">Startzeit:</label>
    
    <div class="form-group">
      <div class="col-md-4">
        <div class='input-group date' id='datetimepicker11'>
                <input type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker11').datetimepicker({
                daysOfWeekDisabled: [0, 6]
            });
        });
    </script>

</div>
    <div class="form-group">
        <label class="col-md-4 control-label" for="textinput">Endzeit:</label>

        <div class="form-group">
            <div class="col-md-4">
                <div class='input-group date' id='datetimepicker10'>
                    <input type='text' class="form-control" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(function () {
                $('#datetimepicker10').datetimepicker({
                    daysOfWeekDisabled: [0, 6]
                });
            });
        </script>

    </div>





<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
    <button id="button1id" name="button1id" class="btn btn-success">Event erstellen</button>
    
  </div>
</div>
</fieldset>
</body>
</html>
