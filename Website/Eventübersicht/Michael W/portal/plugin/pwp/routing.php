<?php

//MENÜAUFBAU -> key: name, der im menü angezeigt wird;
//              value: id auf die geroutet wird
$MENU = array("Menuelement" => "start");

/*
 * Portal::displayTemplate(true); // template anzeigen oder nicht (wenn das Template 'false' ist, dann wird nur der Inhalt des Plugins angezeigt)
 * Portal::addJs(new Js('hallo.js')); // Einbinden von eigenem Javascript - Die JS Datei muss in das Verzeichnis 'static/js' kopiert werden
 * Portal::addCss(new Css('hallo.css')); // Einbinden von eigenem CSS - Die CSS Datei muss in das Verzeichnis 'static/css' kopiert werden
 * Portal::setPage('start.php'); // definiert die Datei, die vom Portal eingebunden werden soll (die Dateien liegen im Verzeichnis 'pages' im plugin verzeichnis
 * Portal::setPagetitle('Test'); // Seitentitel
 */

switch (Portal::getParameterId()) {
        case 'welcome':
            Portal::setPage('welcome.php');
            break;
        case 'welcome_lehrer':
            Portal::setPage('welcome_lehrer.php');
            break;
        case 'createevent':
            Portal::setPage('createevent.php');
            break;
        case 'classoverview':
            Portal::setPage('classoverview.php');
            break;
        case 'detailedview':
            Portal::setPage('detailedview.php');
            break;
        case 'dayinfo':
            Portal::setPage('dayinfo.php');
            break;
    case 'drag':
        Portal::setPage('draganddrop.php');
        break;
        default:
            Portal::displayTemplate(true);
            Portal::addJs(new Js('hallo.js'));
            Portal::addCss(new Css('hallo.css'));
            Portal::setPage('welcome.php');
            break;
}