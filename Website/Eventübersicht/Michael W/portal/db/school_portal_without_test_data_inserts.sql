SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `school_portal` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `school_portal` ;

-- -----------------------------------------------------
-- Table `school_portal`.`Person`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Person` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Vorname` VARCHAR(45) NULL ,
  `Nachname` VARCHAR(45) NULL ,
  `Email` VARCHAR(45) NULL ,
  `Passwort` VARCHAR(64) NULL ,
  `Geschlecht` ENUM('m', 'w') NULL ,
  `Benutzername` VARCHAR(45) NULL ,
  `Typ` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Lehrer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Lehrer` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Kuerzel` VARCHAR(45) NULL ,
  `Person_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Lehrer_Person1_idx` (`Person_id` ASC) ,
  CONSTRAINT `fk_Lehrer_Person1`
    FOREIGN KEY (`Person_id` )
    REFERENCES `school_portal`.`Person` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Abteilung`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Abteilung` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `Lehrer_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Abteilung_Lehrer1_idx` (`Lehrer_id` ASC) ,
  CONSTRAINT `fk_Abteilung_Lehrer1`
    FOREIGN KEY (`Lehrer_id` )
    REFERENCES `school_portal`.`Lehrer` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Klasse`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Klasse` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Jahrgang` INT NULL ,
  `Bezeichnung` VARCHAR(45) NULL ,
  `Sprecher_id` INT NULL ,
  `SprecherStv_id` INT NULL ,
  `LVorstand_id` INT NOT NULL ,
  `Abteilung_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Klasse_Schueler1_idx` (`Sprecher_id` ASC) ,
  INDEX `fk_Klasse_Schueler2_idx` (`SprecherStv_id` ASC) ,
  INDEX `fk_Klasse_Lehrer1_idx` (`LVorstand_id` ASC) ,
  INDEX `fk_Klasse_Abteilung1_idx` (`Abteilung_id` ASC) ,
  CONSTRAINT `fk_Klasse_Schueler1`
    FOREIGN KEY (`Sprecher_id` )
    REFERENCES `school_portal`.`Schueler` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Klasse_Schueler2`
    FOREIGN KEY (`SprecherStv_id` )
    REFERENCES `school_portal`.`Schueler` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Klasse_Lehrer1`
    FOREIGN KEY (`LVorstand_id` )
    REFERENCES `school_portal`.`Lehrer` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Klasse_Abteilung1`
    FOREIGN KEY (`Abteilung_id` )
    REFERENCES `school_portal`.`Abteilung` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Schueler`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Schueler` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `MatNr` VARCHAR(10) NULL ,
  `Person_id` INT NOT NULL ,
  `Klasse_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Schueler_Person_idx` (`Person_id` ASC) ,
  INDEX `fk_Schueler_Klasse1_idx` (`Klasse_id` ASC) ,
  CONSTRAINT `fk_Schueler_Person`
    FOREIGN KEY (`Person_id` )
    REFERENCES `school_portal`.`Person` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Schueler_Klasse1`
    FOREIGN KEY (`Klasse_id` )
    REFERENCES `school_portal`.`Klasse` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Direktor`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Direktor` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Person_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Direktor_Person1_idx` (`Person_id` ASC) ,
  CONSTRAINT `fk_Direktor_Person1`
    FOREIGN KEY (`Person_id` )
    REFERENCES `school_portal`.`Person` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Admin`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Admin` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `Person_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Admin_Person1_idx` (`Person_id` ASC) ,
  CONSTRAINT `fk_Admin_Person1`
    FOREIGN KEY (`Person_id` )
    REFERENCES `school_portal`.`Person` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `school_portal`.`Lehrer_has_Klasse`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `school_portal`.`Lehrer_has_Klasse` (
  `Lehrer_id` INT NOT NULL ,
  `Klasse_id` INT NOT NULL ,
  INDEX `fk_Lehrer_has_Klasse_Klasse1_idx` (`Klasse_id` ASC) ,
  INDEX `fk_Lehrer_has_Klasse_Lehrer1_idx` (`Lehrer_id` ASC) ,
  CONSTRAINT `fk_Lehrer_has_Klasse_Lehrer1`
    FOREIGN KEY (`Lehrer_id` )
    REFERENCES `school_portal`.`Lehrer` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Lehrer_has_Klasse_Klasse1`
    FOREIGN KEY (`Klasse_id` )
    REFERENCES `school_portal`.`Klasse` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `school_portal` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
