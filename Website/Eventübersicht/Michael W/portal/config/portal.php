<?php

class Portal extends Config {

    private static $plugin = '';
    private static $pagetitle = '';
    private static $page = '';
    private static $content = '';
    private static $javascript = array();
    private static $css = array();
    private static $menuelements = array();
    private static $template = true;
    private static $pluginDisplayName = '';

    /*
     * Set name of module, that proceeds the request
     * @param   string  PluginName
     * @return  boolean Is PluginName enabled
     */
    public static function setCallingPlugin($pluginName) {
        if ($pluginName == 'error') { //error handling - no real plugin
            self::$plugin = 'error';
            return true;
        }
        if (in_array($pluginName, self::$enabledPlugins)) {
            self::$plugin = $pluginName;
            return true;
        }
        return false;
    }

    /*
     * Set the Pagetitle of the portal
     * @param   string  Title of Page
     */
    public static function setPagetitle($title) {
        self::$pagetitle = $title;
    }

    /*
     * Set page to be proceed
     * @param   string  File that should be proceed
     */
    public static function setPage($file) {
        self::$page = $file;
    }

    /*
     * Set name of plugin - this name will be shown to the user as a root menu element
     * @param   string  Name of the plugin
     */
    public static function setPluginDisplayName($name) {
        self::$pluginDisplayName = $name;
    }

    /*
     * Set main content that should be displayed instead of page file
     * @param   string  HTML that should be displayed
     */
    public static function setContent($text) {
        self::$content = $text;
    }

    /*
     * Append HTML to main content that should be displayed instead of page file
     * @param   string  HTML that should be displayed
     */
    public static function appendContent($text) {
        self::$content .= $text;
    }

    /*
     * Get all enabled plugins
     * @return  array   All plugins that are enabled in Config Class
     */
    public static function getEnabledPlugins() {
        return self::$enabledPlugins;
    }

    /*
     * Get title of current page
     * @return  string  Title of page
     */
    public static function getPagetitle() {
        return self::$pagetitle;
    }

    /*
     * Get active plugin
     * @return  string  Name of plugin
     */
    public static function getPlugin() {
        return self::$plugin;
    }

    /*
     * Get content of page (Displays content or includes php page of the current plugin)
     */
    public static function getPageContent() {
        if (self::$content != '')
            echo self::$content . PHP_EOL;
        else {
            if (self::$page != '' && file_exists(Settings::getPluginDirectory('pages') . self::$page))
                include(Settings::getPluginDirectory('pages') . self::$page);
            else
                echo Error::DisplayError(404, self::$plugin, self::$page) . PHP_EOL;
        }
    }

    /*
     * Get/Set if the template should be shown (Default: true)
     * @param   boolean Should template be shown?
     * @return  boolean Should template be shown?
     */
    public static function displayTemplate($display = '') {
        if ($display != '')
            self::$template = $display;
        else
            return self::$template;
    }

    /*
     * Checks if there are menuelements and display the menu if there are elements
     * @return  boolean Display menu
     */
    public static function displayMenu() {
            return (count(self::$menuelements) > 0) ? true : false;
    }

    /*
     * Return GET id to simplify switch in the plugin routing
     * @return  string  Value of GET parameter id
     */
    public static function getParameterId() {
        return (isset($_GET['id'])) ? strtolower($_GET['id']) : '';
    }

    /*
     * Returns the HTTP method of the current request
     * @return  string  HTTP Method
     */
    public static function getRequestMethod() {
        $supportedMethods = array('GET', 'POST', 'PUT', 'DELETE', 'HEAD');
        return (in_array($_SERVER['REQUEST_METHOD'], $supportedMethods)) ? $_SERVER['REQUEST_METHOD'] : '';
    }

    /*
     * Add custom css to the template
     * @param   Css Css instance of the new css file
     */
    public static function addCss(Css $css) {
        array_push(self::$css, $css);
    }

    /*
     * Add custom js to the template
     * @param   Js  Js instance of the new js file
     */
    public static function addJs(Js $js) {
        array_push(self::$javascript, $js);
    }

    /*
     * Returns the whole custom css array
     * @return  array   custom css of plugin
     */
    public static function getCss() {
        return self::$css;
    }

    /*
     * Returns the whole custom js array
     * @return  array   custom js of plugin
     */
    public static function getJs() {
        return self::$javascript;
    }
}