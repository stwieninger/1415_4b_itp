<?php

class Css {
    private $file = '';
    private $access = '';
    const PORTAL_PLUGIN = 'plugin';
    const PORTAL_GLOBAL = 'global';

    public function Css($file, $access = Css::PORTAL_PLUGIN) {
        $this->file = $file;
        $this->access = $access;
    }

    public function __toString() {
        if ($this->access == 'plugin')
            return '<link href="' . Settings::getAbsolutePluginUrlToFile('css', $this->file) . '" rel="stylesheet">' . PHP_EOL;
        else
            return '<link href="' . Settings::getAbsoluteStaticUrlToFile('css', $this->file) . '" rel="stylesheet">' . PHP_EOL;
    }
}