<?php

class Js {
    private $file = '';
    private $access = '';
    const PORTAL_PLUGIN = 'plugin';
    const PORTAL_GLOBAL = 'global';

    public function Js($file, $access = Js::PORTAL_PLUGIN) {
        $this->file = $file;
        $this->access = $access;
    }

    public function __toString() {
        if ($this->access == 'plugin')
            return '<script type="text/javascript" src="' . Settings::getAbsolutePluginUrlToFile('js', $this->file) . '"></script>' . PHP_EOL;
        else
            return '<script type="text/javascript" src="' . Settings::getAbsoluteStaticUrlToFile('js', $this->file) . '"></script>' . PHP_EOL;
    }
}