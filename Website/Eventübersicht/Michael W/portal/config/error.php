<?php

class Error {

    public static function DisplayError($code, $plugin, $message = '') {
        return '<h1>Error ' . $code . ' - ' .
        (($message != '') ?
            'File ' . $message . ' of plugin ' . $plugin . ' not found!</h1>' . PHP_EOL :
            'Plugin ' . $plugin . ' not found!</h1>' . PHP_EOL);
    }

    public static function DisplayErrorMessage($message = '') {
        return '<h1>Error: ' . $message . '</h1>' . PHP_EOL;
    }
}