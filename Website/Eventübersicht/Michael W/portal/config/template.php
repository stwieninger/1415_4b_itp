<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>School Portal</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
    <link href="<?php echo Settings::getAbsoluteStaticUrlToFile('css', 'bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo Settings::getAbsoluteStaticUrlToFile('css', 'nifty.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo Settings::getAbsoluteStaticUrlToFile('plugins/font-awesome/css', 'font-awesome.min.css'); ?>" rel="stylesheet">

    <link href="<?php echo Settings::getAbsoluteStaticUrlToFile('plugins/switchery', 'switchery.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo Settings::getAbsoluteStaticUrlToFile('plugins/bootstrap-select', 'bootstrap-select.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo Settings::getAbsoluteStaticUrlToFile('css', 'custom-style.css'); ?>" rel="stylesheet">
    <?php foreach (Portal::getCss() as $elem) echo $elem; ?>
</head>
<body>
<div id="container" class="<?php echo (Portal::displayMenu()) ? 'mainnav-lg' : ''; ?>">

    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <div id="navbar-container" class="boxed">
            <div class="navbar-header">
                <a href="<?php echo Settings::getAbsoluteUrl(''); ?>" class="navbar-brand">
                    <img src="<?php echo Settings::getAbsoluteStaticUrlToFile('img', 'logo.png'); ?>" alt="Logo" class="brand-icon">

                    <div class="brand-title">
                        <span class="brand-text">HTL Krems</span>
                    </div>
                </a>
            </div>

            <!--Navbar Dropdown-->
            <!--================================-->
            <div class="navbar-content clearfix">
                <?php if (Portal::displayMenu()): ?>
                <ul class="nav navbar-top-links pull-left">

                    <!--Navigation toogle button-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="tgl-menu-btn">
                        <a class="mainnav-toggle" href="#">
                            <i class="fa fa-navicon fa-lg"></i>
                        </a>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End Navigation toogle button-->
                </ul>
                <?php endif; ?>
                <?php if (Auth::isLoggedIn()): ?>
                <ul class="nav navbar-top-links pull-right">
                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
								<span class="pull-right">
									<img class="img-circle img-user media-object" src="<?php echo Settings::getAbsoluteStaticUrlToFile('img', 'av1.png'); ?>"
                                         alt="Profile Picture">
								</span>
                            <div class="username hidden-xs">John Tester</div>
                        </a>

                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow panel-default">
                            <ul class="head-list">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user fa-fw fa-lg"></i> Profile
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="badge badge-danger pull-right">9</span>
                                        <i class="fa fa-envelope fa-fw fa-lg"></i> Messages
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="label label-success pull-right">New</span>
                                        <i class="fa fa-gear fa-fw fa-lg"></i> Settings
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-question-circle fa-fw fa-lg"></i> Help
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-lock fa-fw fa-lg"></i> Lock screen
                                    </a>
                                </li>
                            </ul>
                            <div class="pad-all text-right">
                                <a href="pages-login.html" class="btn btn-primary">
                                    <i class="fa fa-sign-out fa-fw"></i> Logout
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
                <?php else: ?>
                <ul class="nav navbar-top-links pull-right">
                    <li id="dropdown-user" class="dropdown">
                        <a href="?modul=login" class="dropdown-toggle text-right">
								<span class="pull-right">
                                    <i class="fa fa-lock fa-2x login-lock"></i>
								</span>
                            <div class="username hidden-xs">Login</div>
                        </a>
                    </li>
                </ul>
                <?php endif; ?>
            </div>
            <!--================================-->
            <!--End Navbar Dropdown-->

        </div>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->

    <div class="boxed">
        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-title">
                <h1 class="page-header text-overflow"><?php echo Portal::getPagetitle(); ?></h1>
            </div>
            <!-- <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Library</a></li>
                <li class="active">Data</li>
            </ol> -->
            <div id="page-content">
            <?php Portal::getPageContent(); ?>
            </div>
        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->

        <?php if (Portal::displayMenu()): ?>
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <div id="mainnav">

                <!--Shortcut buttons-->
                <!--================================-->
                <div id="mainnav-shortcut">
                    <ul class="list-unstyled">
                        <li class="col-xs-4" data-content="Additional Sidebar">
                            <a id="demo-toggle-aside" class="shortcut-grid" href="#">
                                <i class="fa fa-magic"></i>
                            </a>
                        </li>
                        <li class="col-xs-4" data-content="Notification">
                            <a id="demo-alert" class="shortcut-grid" href="#">
                                <i class="fa fa-bullhorn"></i>
                            </a>
                        </li>
                        <li class="col-xs-4" data-content="Page Alerts">
                            <a id="demo-page-alert" class="shortcut-grid" href="#">
                                <i class="fa fa-bell"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--================================-->
                <!--End shortcut buttons-->


                <!--Menu-->
                <!--================================-->
                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content">
                            <ul id="mainnav-menu" class="list-group">

                                <!--Category name-->
                                <li class="list-header">Navigation</li>

                                <!--Menu list item-->
                                <li>
                                    <a href="index.html">
                                        <i class="fa fa-dashboard"></i>
											<span class="menu-title">
												<strong>Dashboard</strong>
												<span class="label label-success pull-right">Top</span>
											</span>
                                    </a>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-th"></i>
											<span class="menu-title">
												<strong>Layouts</strong>
											</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="layouts-collapsed-navigation.html">Collapsed Navigation</a></li>
                                        <li><a href="layouts-offcanvas-navigation.html">Off-Canvas Navigation</a></li>
                                        <li><a href="layouts-offcanvas-slide-in-navigation.html">Slide-in Navigation</a>
                                        </li>
                                        <li><a href="layouts-offcanvas-revealing-navigation.html">Revealing
                                                Navigation</a></li>
                                        <li class="list-divider"></li>
                                        <li><a href="layouts-aside-right-side.html">Aside on the right side</a></li>
                                        <li><a href="layouts-aside-left-side.html">Aside on the left side</a></li>
                                        <li><a href="layouts-aside-bright-theme.html">Bright aside theme</a></li>
                                        <li class="list-divider"></li>
                                        <li><a href="layouts-fixed-navbar.html">Fixed Navbar</a></li>
                                        <li><a href="layouts-fixed-footer.html">Fixed Footer</a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="widgets.html">
                                        <i class="fa fa-flask"></i>
											<span class="menu-title">
												<strong>Widgets</strong>
												<span class="pull-right badge badge-warning">9</span>
											</span>
                                    </a>
                                </li>

                                <li class="list-divider"></li>

                                <!--Category name-->
                                <li class="list-header">Components</li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-briefcase"></i>
                                        <span class="menu-title">UI Elements</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="ui-buttons.html">Buttons</a></li>
                                        <li><a href="ui-checkboxes-radio.html">Checkboxes &amp; Radio</a></li>
                                        <li><a href="ui-panels.html">Panels</a></li>
                                        <li><a href="ui-modals.html">Modals</a></li>
                                        <li><a href="ui-progress-bars.html">Progress bars</a></li>
                                        <li><a href="ui-components.html">Components</a></li>
                                        <li><a href="ui-typography.html">Typography</a></li>
                                        <li><a href="ui-list-group.html">List Group</a></li>
                                        <li><a href="ui-tabs-accordions.html">Tabs &amp; Accordions</a></li>
                                        <li><a href="ui-alerts-tooltips.html">Alerts &amp; Tooltips</a></li>
                                        <li><a href="ui-helper-classes.html">Helper Classes</a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-edit"></i>
                                        <span class="menu-title">Forms</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="forms-general.html">General</a></li>
                                        <li><a href="forms-components.html">Components</a></li>
                                        <li><a href="forms-validation.html">Validation</a></li>
                                        <li><a href="forms-wizard.html">Wizard</a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-table"></i>
                                        <span class="menu-title">Tables</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="tables-static.html">Static Tables</a></li>
                                        <li><a href="tables-bootstrap.html">Bootstrap Tables</a></li>
                                        <li><a href="tables-datatable.html">Data Tables<span
                                                    class="label label-info pull-right">New</span></a></li>
                                        <li><a href="tables-footable.html">Foo Tables<span
                                                    class="label label-info pull-right">New</span></a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="charts.html">
                                        <i class="fa fa-line-chart"></i>
                                        <span class="menu-title">Charts</span>
                                    </a>
                                </li>

                                <li class="list-divider"></li>

                                <!--Category name-->
                                <li class="list-header">Extra</li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-plug"></i>
											<span class="menu-title">
												Miscellaneous
												<span class="label label-mint pull-right">New</span>
											</span>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="misc-calendar.html">Calendar</a></li>
                                        <li><a href="misc-maps.html">Google Maps</a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-envelope"></i>
                                        <span class="menu-title">Email</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="mailbox.html">Inbox</a></li>
                                        <li><a href="mailbox-message.html">View Message</a></li>
                                        <li><a href="mailbox-compose.html">Compose Message</a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li class="active-sub">
                                    <a href="#">
                                        <i class="fa fa-file"></i>
                                        <span class="menu-title">Pages</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse in">
                                        <li class="active-link"><a href="pages-blank.html">Blank Page</a></li>
                                        <li><a href="pages-profile.html">Profile</a></li>
                                        <li><a href="pages-search-results.html">Search Results</a></li>
                                        <li><a href="pages-timeline.html">Timeline<span
                                                    class="label label-info pull-right">New</span></a></li>
                                        <li><a href="pages-faq.html">FAQ</a></li>
                                        <li class="list-divider"></li>
                                        <li><a href="pages-404.html">404 Error</a></li>
                                        <li><a href="pages-500.html">500 Error</a></li>
                                        <li class="list-divider"></li>
                                        <li><a href="pages-login.html">Login</a></li>
                                        <li><a href="pages-register.html">Register</a></li>
                                        <li><a href="pages-password-reminder.html">Password Reminder</a></li>
                                        <li><a href="pages-lock-screen.html">Lock Screen</a></li>

                                    </ul>
                                </li>

                                <!--Menu list item-->
                                <li>
                                    <a href="#">
                                        <i class="fa fa-plus-square"></i>
                                        <span class="menu-title">Menu Level</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="#">Second Level Item</a></li>
                                        <li><a href="#">Second Level Item</a></li>
                                        <li><a href="#">Second Level Item</a></li>
                                        <li class="list-divider"></li>
                                        <li>
                                            <a href="#">Third Level<i class="arrow"></i></a>

                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="#">Third Level Item</a></li>
                                                <li><a href="#">Third Level Item</a></li>
                                                <li><a href="#">Third Level Item</a></li>
                                                <li><a href="#">Third Level Item</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Third Level<i class="arrow"></i></a>

                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="#">Third Level Item</a></li>
                                                <li><a href="#">Third Level Item</a></li>
                                                <li><a href="#">Third Level Item</a></li>
                                                <li class="list-divider"></li>
                                                <li><a href="#">Third Level Item</a></li>
                                                <li><a href="#">Third Level Item</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <!--================================-->
                <!--End menu-->

            </div>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->
        <?php endif; ?>
    </div>

    <!-- FOOTER -->
    <!--===================================================-->
    <footer id="footer">
        <p class="text-center">&#0169; 2015 HTL Krems</p>
    </footer>
    <!--===================================================-->
    <!-- END FOOTER -->


    <!-- SCROLL TOP BUTTON -->
    <!--===================================================-->
    <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
    <!--===================================================-->


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->

<script type="text/javascript" src="<?php echo Settings::getAbsoluteStaticUrlToFile('js', 'jquery-2.1.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Settings::getAbsoluteStaticUrlToFile('js', 'bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Settings::getAbsoluteStaticUrlToFile('plugins/fast-click', 'fastclick.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Settings::getAbsoluteStaticUrlToFile('js', 'nifty.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Settings::getAbsoluteStaticUrlToFile('plugins/switchery', 'switchery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo Settings::getAbsoluteStaticUrlToFile('plugins/bootstrap-select', 'bootstrap-select.min.js'); ?>"></script>
<?php foreach (Portal::getJs() as $elem) echo $elem; ?>
</body>
</html>