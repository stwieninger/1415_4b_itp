<?php

class Log {

    const LOGIN = 'login';
    const LOGOUT = 'logout';
    const DBERROR = 'dberror';

    public static function logToFile($element, $message) {
        file_put_contents(Settings::getUserLogFile(), date('Y-m-d H:i:s') . ' ; ' . $element . ' ; ' . $message . PHP_EOL, FILE_APPEND);
    }

    public static function logError($element, $class, $function, $message) {
        file_put_contents(Settings::getErrorLogFile(), date('Y-m-d H:i:s') . ' ; ' . $element . ' ; ' . $class . ' - ' . $function .
            ' ; ' . $message . PHP_EOL, FILE_APPEND);
    }
}