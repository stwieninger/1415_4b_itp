<?php

abstract class Config {
    protected static $DBNAME = 'portal'; //Database name
    protected static $DBUSER = 'root'; //Database user
    protected static $DBPASSWORD = ''; //Database password
    protected static $DBHOST = 'localhost'; //Database host
    protected static $ROOTDIR = 'portal/'; //Root directory of portal (IMPORTANT: when rootdir is set, always use a '/' at the end!)
    protected static $enabledPlugins = array('public', 'login', 'test', 'pwp'); //All global enabled plugins
    public static $defaultPlugin = 'public'; //set default plugin - this will be displayed, when no modul parameter is given
}