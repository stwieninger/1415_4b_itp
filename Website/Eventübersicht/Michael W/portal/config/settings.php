<?php

class Settings extends Config {

    /*
     * Returns the absolute path of portal + appended $dir
     * @param   string  Directory that should be appended to the document root path
     * @return  string  Absolute directory path + appended $dir
     */
    public static function getDirectory($dir) {
        if ($dir == '')
            return $_SERVER['DOCUMENT_ROOT'] . ((parent::$ROOTDIR != '') ? parent::$ROOTDIR : '/');

        return $_SERVER['DOCUMENT_ROOT'] . '/' . ((parent::$ROOTDIR != '') ? parent::$ROOTDIR : '') . $dir . '/';
    }

    /*
     * Returns the root directory of the plugin
     * @param   string  Directory in the plugin
     * @return  string  Absolute Path to the plugin root directory
     */
    public static function getPluginDirectory($dir = '') {
        if ($dir == '')
            return Settings::getDirectory('plugin/' . Portal::getPlugin() . '/');

        return Settings::getDirectory('plugin/' . Portal::getPlugin() . '/' . $dir . '/');
    }

    /*
     * Get the directory to a static resource (e.g. css, js, ...)
     * @param   string  Appended directory
     * @return  string  Absolute directory of static folder + appended $dir
     */
    public static function getStaticDirectory($dir) {
        return Settings::getDirectory('static') . $dir . '/';
    }

    /*
     * Get file of the static resource folder (e.g. css, js, ...)
     * @param   string  Folder where you find the resource (e.g. css, js, ...)
     * @param   string  Name of file (e.g. main.js)
     * @return  string  Absolute path to resource file
     */
    public static function getStaticFile($dir, $file) {
        return Settings::getStaticDirectory($dir) . $file;
    }

    /*
     * Get relative url (beginning from /)
     * @param   string  directory that should be appended
     * @return  string  relative url + appended directory
     */
    public static function getRelativeUrl($dir) {
        if ($dir == '')
            return '/' . ((parent::$ROOTDIR != '') ? parent::$ROOTDIR : '');
        return '/' . ((parent::$ROOTDIR != '') ? parent::$ROOTDIR : '') . $dir . '/';
    }

    /*
     * Same as function getRelativeUrl + filename appended
     * @param   string  directory that should be appended
     * @param   string  file that should be appended
     * @return  string  relative url + appended directory + appended file
     */
    public static function getRelativeUrlToFile($dir, $file) {
        return Settings::getRelativeUrl($dir) . $file;
    }

    /*
     * Get absolute url (e.g. http://localhost/...)
     * @param   string  directory that should be appended
     * @return  string  absolute url + appended directory
     */
    public static function getAbsoluteUrl($dir) {
        return ($_SERVER['SERVER_PORT'] == 443) ? 'https' : 'http' . '://' . $_SERVER['SERVER_NAME'] . Settings::getRelativeUrl($dir);
    }

    /*
     * Same as function getAbsoluteUrl + filename appended
     * @param   string  directory that should be appended
     * @param   string  file that should be appended
     * @return  string  absolute url + appended directory + appended file
     */
    public static function getAbsoluteUrlToFile($dir, $file) {
        return Settings::getAbsoluteUrl($dir) . $file;
    }

    /*
     * Get absolute url to static (resource) folder (e.g. http://localhost/static/...)
     * @param   string  directory that should be appended
     * @return  string  absolute url to static/resource directory
     */
    public static function getAbsoluteStaticUrl($dir) {
        return Settings::getAbsoluteUrl('static') . $dir . '/';
    }

    /*
     * Same as function getAbsoluteStaticUrl + filename appended
     * @param   string  directory that should be appended
     * @param   string  file that should be appended
     * @return  string  absolute url + appended directory + appended file
     */
    public static function getAbsoluteStaticUrlToFile($dir, $file) {
        return Settings::getAbsoluteStaticUrl($dir) . $file;
    }

    /*
     * Returns the absolute url to the directory of the plugin
     * @param   string  directory
     * @return  string  Absolute Url to the plugin root directory
     */
    public static function getAbsolutePluginUrl($dir) {
        return Settings::getAbsoluteUrl('plugin/' . Portal::getPlugin() . '/' . $dir);
    }

    /*
     * Same as function getAbsolutePluginUrl + filename appended
     * @param   string  directory that should be appended
     * @param   string  file that should be appended
     * @return  string  absolute url + appended directory + appended file
     */
    public static function getAbsolutePluginUrlToFile($dir, $file) {
        return Settings::getAbsoluteUrl('plugin/' . Portal::getPlugin() . '/' . $dir) . $file;
    }

    /*
     * Returns the PDO Object to access the database
     * @return  PDOObject   PDO Object to access the database
     */
    public static function getPDO() {
        try {
            $pdo = new PDO('mysql:dbname=' . parent::DBNAME . ';charset=utf8;host=' . parent::DBHOST, parent::DBUSER, parent::DBPASSWORD);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            return $pdo;
        } catch (Exception $e) {
            if (ENVIRONMENT == 'development')
                echo $e->getMessage(); //TODO Use Message class for better output
        }

        return null;
    }
}