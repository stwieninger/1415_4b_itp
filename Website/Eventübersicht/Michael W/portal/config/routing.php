<?php

if (isset($_GET['modul'])) {
    if (!Portal::setCallingPlugin($_GET['modul'])) {
        Portal::setContent(Error::DisplayError(404, $_GET['modul'], ''));
        Portal::setCallingPlugin('error');
    }
}
else
    Portal::setCallingPlugin(Config::$defaultPlugin);

if (Portal::getPlugin() != 'error') {
    if (file_exists(Settings::getPluginDirectory() . 'index.php'))
        include(Settings::getPluginDirectory() . 'index.php');
    else
        Portal::appendContent(Error::DisplayErrorMessage('Plugin ' . Portal::getPlugin() . ' has no index file!!'));

    if (file_exists(Settings::getPluginDirectory() . 'routing.php'))
        include(Settings::getPluginDirectory() . 'routing.php');
    else
        Portal::appendContent(Error::DisplayErrorMessage('Plugin ' . Portal::getPlugin() . ' has no routing file!!'));
}

if (Portal::displayTemplate())
    include('config/template.php');
else
    Portal::getPageContent();