<?php
session_start();

date_default_timezone_set('Europe/Vienna');
define('ENVIRONMENT', 'development'); // can be development or production (production won't display errors)

require_once('config/config.php');
require_once('config/settings.php');
require_once('config/types/css.php');
require_once('config/types/js.php');
require_once('config/types/menuelement.php');
require_once('config/portal.php');
//require_once('config/log.php');
require_once('config/error.php');
require_once('config/auth.php');
require_once('config/routing.php');