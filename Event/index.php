<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Events</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="css/styles.css" rel="stylesheet">
	</head>
	<body>
<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
      <a href="./" class="navbar-brand">Home</a>
    </div>
    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li>
          <a href="#sec1">Montag</a>
        </li>
        <li>
          <a href="#sec2">Dienstag</a>
        </li>
        <li>
          <a href="#sec3">Mittwoch</a>
        </li>
        <li>
          <a href="#sec4">Donnerstag</a>
        </li>
        <li>
          <a href="#sec5">Freitag</a>
        </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
	        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
	        <ul class="dropdown-menu">
	          <li><a href="#">Settings</a></li>
	          <li><a href="#">My events</a></li>
	          <li><a href="#">Logout</a></li>
	        </ul>
	      </li>
      </ul>
    </nav>
  </div>
</header>

<div class="header alt vert">
  <div class="container">
    
    <h1>HTL-Zwettl</h1>
      <p class="lead">Eventplanung</p>
      <div>&nbsp;</div>
  </div>
</div>

<div id="sec1" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Montag</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div align="right" id="sec2" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Dienstag</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-12">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div id="sec3" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Mittwoch</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div align="right" id="sec4" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Donnerstag</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-12">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>

<div id="sec5" class="blurb">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <h1>Freitag</h1>
        <p class="lead">The Most Popular Responsive Framework</p>
      </div>
      <div class="col-md-5">
        <h1 class="pull-right"><i class="icon-mobile-phone icon-3x"></i> <i class="icon-tablet icon-3x"></i> <i class="icon-laptop icon-3x"></i></h1>
      </div>
    </div>
  </div>
</div>

<hr>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3 text-center">
        <ul class="list-inline">
          <li><i class="icon-facebook icon-2x"></i></li>
          <li><i class="icon-twitter icon-2x"></i></li>
          <li><i class="icon-google-plus icon-2x"></i></li>
          <li><i class="icon-pinterest icon-2x"></i></li>
        </ul>
        <hr>
        <p>ITP-Gruppe-WIEN</p>
      </div>
    </div>
  </div>
</footer>

<ul class="nav pull-right scroll-down">
  <li><a href="#" title="Scroll down"><i class="icon-chevron-down icon-3x"></i></a></li>
</ul>
<ul class="nav pull-right scroll-top">
  <li><a href="#" title="Scroll to top"><i class="icon-chevron-up icon-3x"></i></a></li>
</ul>

	<!-- script references -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/scripts.js"></script>
	</body>
</html>